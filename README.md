# ExcelToJsonTools
一款能够将指定Excle文件转换为Json格式文本文件的java小工具，源码有详细注释，可扩展

> 2017/9/18
> Foldcc

说明:
这个工具是之前自己的项目中用到的一个操作Excle文件工具，我把它单独提取出来做成了一个小工具，只上传了源码，第三方包没有上传，请自行下载。

## 主要方法:
   
### EJtool.ExcelToJson(String excelFilePath , String jsonFilePath)

> 传入指定的Excel文件路径全名 和 一个待输出的文件路径全名， 将指定路径的Excle文件转换为Json格式的文本文件，转换成功返回true

### EJtool.JsonToExcel(String jsonFilePath , String excelFilePath)

> 传入指定的Json文本文件路径全名 和 一个待输出的Excel文件路径全名， 将指定路径的文件转换为Excle文件，转换成功返回true

演示截图：
![操作截图](https://git.oschina.net/uploads/images/2017/0918/201508_6b85235e_1511066.png "QQ截图20170918201318.png")
![输出文件](https://git.oschina.net/uploads/images/2017/0918/201541_b0a69f13_1511066.png "QQ截图20170918201340.png")