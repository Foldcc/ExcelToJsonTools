package com.Foldcc.EJ_Tools;

import java.io.File;
import java.util.List;
/**
 * 用于操作加密解密文件
 * @author Foldcc
 *
 */
public class EJtool {
	private ExcelUtils excelUtils = new ExcelUtils();
	private JsonTool jsonTool = new JsonTool();
	private FileUtils fileTool = new FileUtils();
	private EncodeAndDecode encodeTool = new EncodeAndDecode();
	/**
	 * 将指定路径下的excel文件转为指定路径文件(路径+文件全名) ，转换成功返回true
	 * @param excelFilePath
	 * @param jsonFilePath
	 * @return
	 */
	public boolean ExcelToJson(String excelFilePath , String jsonFilePath){
		boolean isT = false;
		List<String[]> msg = excelUtils.readExcel(new File(excelFilePath));
		if(msg != null){
			String jsonStr = jsonTool.getJsonWithList(msg);
			if(jsonStr != null && fileTool.createFile(jsonFilePath)){
				fileTool.WriteBase(jsonStr, jsonFilePath);
				isT = true;
			}
		}
		return isT;
	}
	
	/**
	 * 将指定json格式文件转化为指定路径excel文件(路径+文件全名),转换成功返回true
	 * @param jsonFilePath
	 * @param excelFilePath
	 * @return
	 */
	public boolean JsonToExcel(String jsonFilePath , String excelFilePath){
		boolean isT = false;
		String jsonStr = fileTool.ReadBase(jsonFilePath);
		if(jsonStr != null){
			List<String[]> msg = jsonTool.getList(jsonStr);
			if(msg!=null && excelUtils.toExcel(msg, excelFilePath)){
				isT = true;
			}
		}
		return isT;
	}
	
	/**
	 * 将指定加密格式的Json文件转换为Excel文件
	 * @param jsonFilePath
	 * @param excelFilePath
	 * @return
	 */
	public boolean JsonToExcelEncode(String jsonFilePath , String excelFilePath){
		boolean isT = false;
		String jsonStr = fileTool.ReadBase(jsonFilePath);
		jsonStr = encodeTool.decode(jsonStr);
		if(jsonStr != null){
			List<String[]> msg = jsonTool.getList(jsonStr);
			if(msg!=null && excelUtils.toExcel(msg, excelFilePath)){
				isT = true;
			}
		}
		return isT;
	}
	
	/**
	 * 将指定Excle文件转化为加密格式的json文件
	 * @param excelFilePath
	 * @param jsonFilePath
	 * @return
	 */
	public boolean ExcelToJsonEncode(String excelFilePath , String jsonFilePath){
		boolean isT = false;
		List<String[]> msg = excelUtils.readExcel(new File(excelFilePath));
		if(msg != null){
			String jsonStr = jsonTool.getJsonWithList(msg);
			jsonStr = encodeTool.encode(jsonStr);
			if(jsonStr != null && fileTool.createFile(jsonFilePath)){
				fileTool.WriteBase(jsonStr, jsonFilePath);
				isT = true;
			}
		}
		return isT;
	}
}
